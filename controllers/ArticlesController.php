<?php

include_once ROOT.'/models/ArticleModel.php';

class ArticlesController {

    public function getDataArray($temp){

        $items = array();

        while ($row = $temp->fetch_assoc()) {

            $items[]=$row;
        }

        return $items;
    }

    public function getArticles(){

        $articles = new ArticleModel();
        $data =$articles->allArticles();
        $data = $this->getDataArray($data);

        echo json_encode($data);
    }

    public function getItem(){

        $id = $_GET['id'];

        $art = new ArticleModel();
        $article = $art->getArticle($id);
        $article = $this->getDataArray($article);

        echo json_encode($article);

    }

    public function add(){

        $title = $_POST['title'];
        $content = $_POST['content'];

        $art = new ArticleModel();
        $art->addArticle($title, $content);
    }

    public function delete(){

        $id = $_POST['id'];

        $art = new ArticleModel();
        $art->deleteArticle($id);
    }

    public function edit(){

        $id = $_POST['id'];
        $title = $_POST['title'];
        $content = $_POST['content'];

        $art = new ArticleModel();
        $art->editArticle($id, $title, $content);
    }

}


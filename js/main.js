    ur = "vsite";

    function getUsers(){

        $.ajax({
            type: "GET",
            url: "http://"+ ur + "/users/getUsers/",
            dataType: 'json',
            success: function(data){
                console.log('Success GetUsers');
                $('tr.dl').remove();   
                data.forEach( function(el){
                    $('#list_users').append('<tr class="dl table-info"><td> ' + el['name'] + '</td><td>' + el.role + '</td></tr>')
                });
            },
            error: function(data){
                console.log('Error GetUsers!');
            }
        });
    };

    function getArticles(){
        $.ajax({
            type: "GET",
            url: "http://" + ur + "/articles/getArticles/",
            dataType: 'json',
            success: function(data){
                console.log('Success GetArticles');
                console.log('DATA: '+ data[0]['title']);
                $('div.rem').remove();   
                data.forEach( function(el){
                    $('#art_list').append('<div class="rem card" style="width: 28rem;"><div class="card-body"><h5 class="card-title blue text-center">' + el['title'] + '</h5><p class="card-text">' + el['content'] + '</p></div></div>')
                });
            },
            error: function(data){
                console.log('Error GetArticles!');
            }
        });
    };

    /* add article form */
    $( "#article_form" ).dialog({
        autoOpen: false,
        width: 900,
        buttons: [
            {
                id: 'add',
                text: 'Add',
                click: function() {
                    var title = $('#article_title').val();
                    var content = $('#article_content').val();                  
                    $.ajax({
                        type: "POST",
                        url: "http://" + ur + "/articles/add/",
                        data: {
                            title: title,
                            content: content,
                        },
                        success: function(data){
                            $('#article_form').dialog('close');
                            $('div.rem').remove();                
                            getArticles();
                        },
                        error: function(data){
                                console.log('Logout Error');
                        }
                    });
                }
            },
            {
                id: 'edit',
                text: 'Edit',
                click: function() {
                    var article_id = $('#article_id').val();
                    var title = $('#article_title').val();
                    var content = $('#article_content').val();                                   
                    $.ajax({
                        type: "POST",
                        url: "http://" + ur + "/articles/edit/",
                        data: {
                            id: article_id,
                            title: title,
                            content: content
                        },
                        success: function(data){
                            $('div.rem').remove();                                            
                            getArticles();
                            $('#article_form').dialog('close');                            
                        },
                        error: function(data){
                        },
                    });
                }
            },
            {
                id: 'delete',
                text: 'Delete',
                click: function() {
                    var article_id = $('#article_id').val();
                    $.ajax({
                        type: "POST",
                        url: "http://" + ur + "/articles/delete/",
                        data: {
                            id: article_id,
                        },
                        success: function(data){
                            $('div.rem').remove();                                                                        
                            getArticles();
                            $('#article_form').dialog('close');                            
                        },
                        error: function(data){
                        },
                    });
                }
            },
            {
                id: 'dont',
                text: 'Cancel',
                click: function() {
                    $('#article_form').dialog('close');
                }
            }
        ],
        show: {
            effect: "drop",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        }
    });

    $('#add_article_btn').click(function() {
            $('#article_form').dialog('open');
    });

    $('#list_articles_btn').click(function() {
        $('#list_articles').dialog('open');
        $.ajax({
            type: "GET",
            url: "http://" + ur + "/articles/getArticles/",
            dataType: 'json',
            success: function(data){
                console.log('Success GetTitles');
                $('tr.del').remove();
                data.forEach( function(el){
                    $('#title_tr').append('<tr class="del edit_article" data-id='+el['id']+' ><td ><a href="#">' + el['title'] + '</a></td></tr>');
                });
                $('.edit_article').on('click', edit_article)
            },
            error: function(data){
                console.log('Error GetArticles!');
            }
        });
    });

    function edit_article(data){
        id = $(this).data('id');
        $.ajax({
            type: "GET",
            url: "http://" + ur + "/articles/getItem/",
            data: {
                id : id
            }, 
            dataType: 'json',          
            success: function(data){ 
                console.log("Edit_article");
                $('#article_id').val(data[0]['id']);
                $('#article_title').val(data[0]['title']);
                $('#article_content').val(data[0]['content']);
                $( "#article_form" ).dialog('open');
                $( "#list_articles" ).dialog('close');                
            },
            error: function(data){
                console.log(' Error!');
            }   
        });
    };

    $( "#list_articles" ).dialog({
        autoOpen: false,
        width: 900,
    });
    $( "#close_list_btn" ).click(function() {
        $('#list_articles').dialog('close');
    });
    $( "#add_article_btn_list" ).click(function() {
            $('#article_form').dialog('open');
            $('#list_articles').dialog('close');
    });

    $.ajax({
        type: "GET",
        url: "http://" + ur + "/users/userStatus/",

        dataType: 'json',

        success: function(data){
            console.log('Check Auth userStatus');            
            $('#greeting').text('Hello, ' + data['name']);
            getArticles();
            if(data['auth'] === 1 ) {
                $('#login_btn').hide();
                $('#logout_btn').show();
                $('#art_list').show();
            } else {
                $('#logout_btn').hide();
                $('#login_btn').show();
                $('#art_list').hide();                
            }
            if( parseInt(data['role']) == 1 ) {
                $('#admin_btn').show();
                $('#users_list').show();
                getUsers();
            } else {
                $('users_list').hide();
            }
        },
        error: function(data){
                console.log('Error Status');
                $('#logout_alt').hide();
        }
    });   













